const configWindow = {
  alwaysOnTop: true,
  hidden: true
};

chrome.app.runtime.onLaunched.addListener(function() {
  chrome.app.window.create("chat-webview.html", configWindow, function(appWin) {
    appWin.contentWindow.addEventListener("DOMContentLoaded", function(e) {
      var webview = appWin.contentWindow.document.querySelector("webview");
      // webview.src = "https://www.twitch.tv/popout/jonny713/chat";
      webview.src = "https://codepen.io/jonny713/full/GaMZNJ"

        webview.addEventListener('permissionrequest', function(e) {
            if (e.permission === 'media') {
                e.request.allow();
            }
        });

      appWin.show();
    });
  });
});
