const fs = require('fs')
const express = require('express')
const app = express()
const bodyParser = require('body-parser')

// let loggerOpts = { lim: 2, showTrace: true }
const logger = require('./modules/log.js')({ lim: 1, showTrace: false })

const ws = require('./web-socket.js')
const config = require('./config.js')

// Включаем парсер параметров запроса
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// Устанавливаем движок для парсинга
app.set('view engine', 'pug')
app.set('views', './')

// Запускаем прослушивание
let server = app.listen(config.port)
console.log(`Сервер запущен, порт ${config.port}`);
console.log(`CTRL+клик ${config.protocol||'http'}://${config.host||'localhost'}:${config.port}/`);
logger.showTrace = false


// Маршрутизируем запросы в зависимости от доменного имени в запросе
let { domains } = config

app.use((req, res, next) => {
    let flag = false
    let reqHost = req.headers.host.split(':')[0]

    for (let domain in domains) {
        if (reqHost === domain) {
            req.url = `/${domains[domain]}/${req.url}`
            flag = true
            next()
        }
    }
    if (!flag) res.send(`Unknown domain ${reqHost}`)
})

let resources = []
let webSocketHandlers = {}

console.log('Создание ресурсов');
for (let resource in domains) {
  if (resources.includes(domains[resource])) continue

  console.log('Добавление домена ', domains[resource])

  // Папка с публичными файлами
  console.log('\t подключение публичных файлов')
  if (fs.existsSync(`./${domains[resource]}/dist`)){
    app.use(`/${domains[resource]}`, express.static(`./${domains[resource]}/dist`))
  } else if (fs.existsSync(`./${domains[resource]}/public`)){
    app.use(`/${domains[resource]}`, express.static(`./${domains[resource]}/public`))
  } else {
    console.log(`Err:\t ./${domains[resource]}/public отсутствует`)
  }

  // Папка с рендер файлами. Движок для всех файлов используется общий
  console.log('\t подключение view-файлов')
  if (fs.existsSync(`./${domains[resource]}/views`)) {
    app.use(`/${domains[resource]}`, (req, res, next) => {
      res._render = res.render
      res.render = (view, locals, callback) => {
        if (fs.existsSync(`./${domains[resource]}/views/${view}.${app.get('view engine')}`)) {
          res._render(`${domains[resource]}/views/${view}`, locals, callback)
        } else {
          console.log(`Err:\t ./${domains[resource]}/views/${view}.${app.get('view engine')} отсутствует`)
          res.send(resource)
        }
      }
      next()
    })
  } else {
    console.log(`Err:\t ./${domains[resource]}/views отсутствует`)
    app.use(`/${domains[resource]}`, (req, res, next) => {
      res._render = res.render
      res.render = (view, locals, callback) => res.send(resource)
      next()
    })
  }

  // TODO сделать общее логирование

  console.log('\t подключение маршрутов')
  try {
    app.use(`/${domains[resource]}`, require(`./${domains[resource]}`))
  } catch(e) {
    console.log(`Err:\t ./${domains[resource]}/index.js отсутствует`)
    const router = express.Router()
    router.get('*', (req, res) => res.send(resource))
    app.use(`/${domains[resource]}`, router)
  }

  try {
    let { handler } = require(`./${domains[resource]}/ws`)
    console.log('\t подключен вебсокет')
    webSocketHandlers[domains[resource]] = handler
  } catch(e) {
    if (domains[resource] == 'chat')
      console.log(e)
  }

  resources.push(domains[resource])
}

// Подключаем к серверу вебсокет на том же порту
ws(server, webSocketHandlers)
console.log(`Вебсокет запущен, порт ${config.port}`);
