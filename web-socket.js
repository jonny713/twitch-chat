/**
* Общий файл вебсокета, транслирует сообщения модулям
*/

const WebSocket = require('ws')
const assert = require('assert');

module.exports = (server, handlers) => {

    // Инициализируем основной WebSocket-сервер
    let webSocketServer = new WebSocket.Server({server})

    /** При отлавливании нового соединения, ждем сообщения от клиента  вида:
    * {
    *   domain: информация о домене
    *
    * }
    * после ищем клиента среди имеющихся и передаем ему соединение
    */

    webSocketServer.on('connection', ws => {
      // console.dir(ws, {depth: 0, colors: 1})

      ws.__send = ws.send
      ws.send = data => ws.__send(JSON.stringify(data))

      ws.on('message', async message => {
        try {
          message = JSON.parse(message)

          let { domain, data } = message

          console.log(domain, data)

          assert(domain, 'Отстутсвует информация о домене')
          assert(data, 'Отстутсвуют данные')
          assert(handlers[domain] && typeof handlers[domain] === 'function', 'Не назначен обработчик')

          handlers[domain](ws, data)

        } catch(e) {
          console.log('Ошибка', e.message);

          ws.send({
            type: 'error',
            message: 'Неизвестный тип сообщения',
            eMessage: e.message
          })
        }
    })
  })

}
