const fs = require('fs')
const TableType = require('./types/type.js')

// загружаем список файлов из папки types
const files = fs.readdirSync(__dirname + '/types')

for (let file of files) {
  // пропускаем все не .js файлы и файл type.js
  if (file == 'type.js' || !file.includes('.js'))
    continue

  // имя типа берем из имени файла
  let typeName = file.replace('.js', '').toUpperCase()

  try {
    // загружайм файл в виде модуля
    let Type = require('./types/' + file)
    let objType = new Type({})

    // если полученный класс является наследником TableType, добавляем в список
    if (objType instanceof TableType)
      module.exports[typeName] = Type

  } catch (e) {
    console.log(e)

  }
}
