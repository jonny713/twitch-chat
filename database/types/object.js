const TableType = require('./type')

class OBJECT extends TableType {
  constructor(schema) {
    // Данные параметры нельзя переопределять
    let sqlType = 'TEXT'

    // Данные параметры переопределять можно
    let def = typeof schema.default == 'object' ? schema.default : {}

    return super({ ...schema, sqlType, def })
  }

  searchHandler(data) {
    let s = super.searchHandler(data)

    delete s.search

    return s
  }

  loadHandler(data) {
    if (typeof data == 'string' && data[0] == '{') {
      try {
        let res = JSON.parse(data)
        return res
      } catch (e) {
        console.error('Не удалось распарсить значение')
        return {}
      }
    }
    console.log(`Формат значения ${data} не соотвествует типу OBJECT`)
    return {}
  }

  saveHandler(data) {
    if (typeof data == 'object') {
      try {
        let res = `'${JSON.stringify(data)}'`
        return res
      } catch (e) {
        console.error('Не удалось преобразовать в JSON')
        return ''
      }
    }
    console.log(`Формат значения ${data} не соотвествует типу OBJECT`)
    return ''
  }
}

module.exports = OBJECT
