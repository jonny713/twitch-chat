const TableType = require('./type')

// функция для экранирования строк
function esc(str) {
  return str.replace(/\"/g, '\\"')
}

class TEXT extends TableType {
  constructor(schema) {
    // Данные параметры нельзя переопределять
    let sqlType = 'TEXT'

    // Данные параметры переопределять можно
    let def = typeof schema.default == 'string' ? schema.default : null

    return super({ ...schema, sqlType, def })
  }

  searchHandler(data) {
    let s = super.searchHandler(data)

    if (typeof s.search == 'string') {
      s.search = `"${esc(data)}"`
    } else {
      delete s.search
    }

    return s
  }

  loadHandler(data) {
    return data
  }

  saveHandler(data) {
    return `'${esc(data)}'`
  }
}

module.exports = TEXT
