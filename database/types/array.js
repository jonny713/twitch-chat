const TableType = require('./type')

class ARRAY extends TableType {
  constructor(schema) {
    // Данные параметры нельзя переопределять
    let sqlType = 'TEXT'

    // Данные параметры переопределять можно
    let def = Array.isArray(schema.default) ? schema.default : []

    super({ ...schema, sqlType, def })

    return this
  }

  searchHandler(data) {
    let s = super.searchHandler(data)

    delete s.search

    return s
  }

  loadHandler(data) {
    if (typeof data == 'string' && data[0] == '[') {
      try {
        let res = JSON.parse(data)
        return res
      } catch (e) {
        console.error('Не удалось распарсить значение')
        return []
      }
    }
    console.log(`Формат значения ${data} не соотвествует типу ARRAY`)
    return []
  }

  saveHandler(data) {
    if (typeof data == 'object' && Array.isArray(data)) {
      try {
        let res = `'${JSON.stringify(data)}'`
        return res
      } catch (e) {
        console.error('Не удалось преобразовать в JSON')
        return ''
      }
    }
    // console.error(data)
    console.log(`Формат значения ${data} не соотвествует типу ARRAY`)
    return ''
  }
}

module.exports = ARRAY
