const TableType = require('./type')

class INTEGER extends TableType {
  constructor(schema) {
    // Данные параметры нельзя переопределять
    let sqlType = 'INTEGER'

    // Данные параметры переопределять можно
    let def = typeof schema.default === 'number' ? schema.default : null

    return super({ ...schema, sqlType, def })
  }

  searchHandler(data) {
    let s = super.searchHandler(data)

    if (typeof s.search == 'number') {
      s.search = `${data}`
    } else {
      delete s.search
    }

    return s
  }
}

module.exports = INTEGER
