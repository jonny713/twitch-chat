const TableType = require('./type')

class ID extends TableType {
  constructor() {
    // Данные параметры нельзя переопределять
    let sqlType = 'INTEGER'
    let primaryKey = true
    let notNull = true
    let autoincrement = true
    let unique = true

    return super({ sqlType, primaryKey, notNull, autoincrement, unique })
  }

  searchHandler(data) {
    let s = super.searchHandler(data)

    delete s.column

    return s
  }
}

module.exports = ID
