
class TableType {
  constructor({ sqlType = 'INTEGER',  primaryKey,  notNull,  autoincrement,  unique,  def = null }) {
    this.sqlType = sqlType
    this.primaryKey = !!primaryKey
    this.notNull = !!notNull
    this.autoincrement = !!autoincrement
    this.unique = !!unique
    this.default = def

    return
  }

  /**
  * выдает отформатированную строку с sql типом и параметрами
  * в качестве входных данных получает переопределения для параметров для данного запроса
  */
  sql() {
    let sql = this.sqlType

    if (this.notNull)
      sql += ' NOT NULL'
    if (this.primaryKey)
      sql += ' PRIMARY KEY'
    if (this.primaryKey && this.autoincrement)
      sql += ' AUTOINCREMENT'
    if (this.unique)
      sql += ' UNIQUE'
    if (this.default !== undefined && this.default !== null)
      sql += ' DEFAULT ' + this.saveHandler(this.default) //(typeof this.default == 'object' ? '\'' + JSON.stringify(this.default) + '\'' : this.default)

    return sql
  }

  /*
  */

  search(){}

  searchHandler(data) {
    return {
      column: true,
      search: data
    }
  }
  loadHandler(data) {
    return data
  }
  saveHandler(data) {
    return data
  }
}

module.exports = TableType
