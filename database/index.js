/*
* Реализация класса для доступа к данным sqlite
* создается подключение к указанному файлу базы данных
* поддерживаются методы получения, вставки, изменения и удаления данных
*/
// TODO: добавить метод резервирующий файл бд

const sqlite = require('sqlite3').verbose()
const isLog = false

// TODO: вынести в конфиг
const DB_NAME = './hoes.db'

// функция для экранирования строк
function esc(str) {
  return str.replace(/\"/g, '\\"')
}


/**
* Рекурсивное создание параметров запроса SELECT и join'ы других таблиц
* @table имя таблицы
* @headers массив столбцов таблицы
* @row объект с параметрами запроса
* @cols массив, в который будут помещены итоговые результаты
* @joins массив, в который будут помещены join'ы других таблиц
*/
function selectQueryBuilder(table, tables, row, cols = [], joins = [], searchs = []) {
  for (let header of tables[table].columns) {
    if (typeof row[header.name] == 'string')
      searchs.push(`${table}.${header.name}="${esc(row[header.name])}"`)

    if (typeof row[header.name] == 'number' && typeof row[header.name] == 'boolean')
      searchs.push(`${table}.${header.name}=${row[header.name]}`)

    if (header.name.includes('Id')) {
      let joinedTable = header.name.replace('Id', '')
      joins.push(`JOIN ${joinedTable} ON ${table}.${header.name}=${joinedTable}.id`)
      if (typeof row[joinedTable] == 'object') {
        cols.push(...tables[joinedTable].columns.map(h => `${joinedTable}.${h} as ${joinedTable}_${h}`))
        selectQueryBuilder(joinedTable, tables, row[joinedTable], cols, joins, searchs)
      }
    }
  }
}

// console.log('Соединение с базой данных успешно установлено');

/**
* Класс предназначенный для работы непосредственно с подключением к базе данны
*/
class DB {
  /**
  * Создает подключение к указанной таблице или возвращает ранее созданное
  * @dbName имя таблицы
  */
  static connect({ dbName = DB_NAME } = {}) {
    // Проверяем, что поле all существует и ищем там ранее созданное подключение к бд
    if (DB.all && DB.all[dbName])
      return DB.all[dbName]

    // Создаем подключение
    let db = new DB(dbName)

    // Сохранение подключения в списке
    DB.all = DB.all || {}
    DB.all[dbName] = db

    return db
  }

  constructor(dbName) {
    this.ready = false
    this.init(dbName)
    return this
  }

  async isReady() {
    if (!this.ready) {
      console.log('Подключение к БД подготавливается')
      return new Promise( (res, rej) => setTimeout(_ => res(this.isReady), 50) )
    }
  }

  /*
  * Актуализируем список таблиц в БД со списком в объекте
  */
  async updateTablesList() {
    try {
      // Достаем из бд список всех имеющихся таблиц
      let tables = await new Promise( (res, rej) => this._db.all(`SELECT * FROM SQLITE_MASTER`, (err, rows) => err ? rej(err) : res(rows) ) )
      tables = tables.filter(t => t.type == 'table')

      this.tables = {}
      for (let table of tables) {
        try {
          this.tables[table.name] = {
            columns:  table.sql.replace(/[^(]+/, '').match(/"[^"]+"[^")]+/g).map(s => ({
              name: s.match(/"([^"]+)"/)[1],
              type: s.match(/"\s([^\s,]+)[\s,]/)[1],
              primaryKey: s.includes('PRIMARY KEY'),
              notNull: s.includes('NOT NULL'),
              autoincrement: s.includes('AUTOINCREMENT'),
              unique: s.includes('UNIQUE'),
              default: s.includes('DEFAULT') ? s.replace(/.*(DEFAULT (\d+)|DEFAULT '([^']+)')[\s\S]*/, '$2$3') : undefined
            })),
            sql: table.sql
          }
        } catch (e) {
          console.log(`${table.name} проигнорирована`);
        }
      }
    } catch (e) {

    }
  }

  async init(dbName){
    try {
      // создаем подключение
      this._db = await new Promise( (res, rej) => {
        let db = new sqlite.Database(dbName, err => err ? rej(err) : res(db))
      })

      await this.updateTablesList()

      // // Достаем из бд список всех имеющихся таблиц
      // let tables = await new Promise( (res, rej) => this._db.all(`SELECT * FROM SQLITE_MASTER`, (err, rows) => err ? rej(err) : res(rows) ) )
      // tables = tables.filter(t => t.type == 'table')
      //
      // this.tables = {}
      // for (let table of tables) {
      //   // let headers = table.sql.match(/\n\t"[^"]+"/g)
      //   // if (headers !== null)
      //   // this.tables[table.name] = {
      //   //   columns: headers.map(h => h.replace(/\n\t/, '').replace(/"/g, '')),
      //   //   sql: table.sql
      //   // }
      //
      //   try {
      //     this.tables[table.name] = {
      //       columns:  table.sql.replace(/[^(]+/, '').match(/"[^"]+"[^")]+/g).map(s => ({
      //         name: s.match(/"([^"]+)"/)[1],
      //         type: s.match(/"\s([^\s,]+)[\s,]/)[1],
      //         primaryKey: s.includes('PRIMARY KEY'),
      //         notNull: s.includes('NOT NULL'),
      //         autoincrement: s.includes('AUTOINCREMENT'),
      //         unique: s.includes('UNIQUE'),
      //         default: s.includes('DEFAULT') ? s.replace(/.*(DEFAULT (\d+)|DEFAULT '([^']+)')[\s\S]*/, '$2$3') : undefined
      //       })),
      //       sql: table.sql
      //     }
      //   } catch (e) {
      //     console.log(`${table.name} проигнорирована`);
      //
      //   }
      // }

      this.ready = true
      console.log('Соединение с базой данных успешно установлено')
      // console.log(this.tables);

      // возвращаем
      return this
    } catch (e) {
      console.log('Соединение с базой данных установит не удалось')
      console.log(e)
      return
    }
  }

  // Проверка наличия таблицы
  checkTable(table) {
    // Ищем таблицу среди имеющихся
    if (!(table in this.tables)) {
      console.log(`Таблицы с именем ${table} не существует`)
      throw Error(`Таблицы с именем ${table} не существует`)
    }
    return this.tables[table]
  }

  async request({ query = '' }) {
    // ожидаем готовности подключения
    await this.isReady()

    let result = await new Promise( (res, rej) => this._db.all(query, (err, rows) => err ? rej(err) : res(rows) ) )

    return result
  }

  /**
  * Возвращает список всех подходящих записей. JOIN происходит автоматически по наличию в имени столбца 'Id'
  * @param table имя таблицы
  * @param search объект поиска, содержащий параметры двух типов:
  ** col: поиск записей где имеется столбец с указанным значением
  ** table: объект, аналогичный search, выполняющий поиск по JOINED таблицам
  * @return Array - массив с полученными данными. Если данные не найдены - пустой массив
  */
  async select({ table = '', search = {} }) {
    // ожидаем готовности подключения
    await this.isReady()

    // проверяем имя таблицы
    this.checkTable(table)

    // формируем join'ы и строку поиска с помощью рекурсивной функции
    let cols = [...this.tables[table].columns.map(h => `${table}.${h.name}`)]
    let joins = []
    let searchs = []
    selectQueryBuilder(table, this.tables, search, cols, joins, searchs)

    // формируем строку запроса
    let query = `SELECT ${cols.join(', ')} FROM ${table}${joins.length ? ' INNER ' + joins.join(' ') : ''}${searchs.length ? ' WHERE ' + searchs.join(' and ') : ''}`
    console.log('query: ', query)

    // выполняем запрос
    let result = await new Promise( (res, rej) => this._db.all(query, (err, rows) => err ? rej(err) : res(rows) ) )

    // преобразуем все поля с _ в объекты
    result = result.map(res => {
      for (let p in res) {
        if (p.includes('_')) {
          let name = p.split('_')[0]
          let key = p.split('_')[1]
          res[name] = res[name] || {}
          res[name][key] = res[p]
          delete res[p]
        }
      }
      return res
    })
    return result
  }

  /**
  * Возвращает первую подходящую запись. JOIN происходит автоматически по наличию в имени столбца 'Id'
  * @param table имя таблицы
  * @param search объект поиска, содержащий параметры двух типов:
  ** col: поиск записей где имеется столбец с указанным значением
  ** table: объект, аналогичный search, выполняющий поиск по JOINED таблицам
  * @return Object - конкретный объект с данными или undefined, если данные не найдены
  */
  async selectOne({ table = '', search = {} }) {
    let arr = await this.select({ table, search })

    return arr[0]
  }

  /**
  * Вставляет указанную запись в указанную таблицу
  * @param table имя таблицы
  * @param row объект, который необходимо вставить
  * @return result - объект с результатом выполнения
  */
  // TODO: добавить обработку результата выполнения
  async insert({ table, row = {} }) {
    // ожидаем готовности подключения
    await this.isReady()

    // проверяем имя таблицы
    this.checkTable(table)

    // формируем массивы столбцов и значений для них
    let cols = []
    let values = []
    for (let header of this.tables[table].columns) {
      // столбец id контроллируется самой СУБД, поэтому исключаем его
      if (header.name == 'id')
        continue

      if (header.name in row) {
        cols.push(header.name)
        if (typeof row[header.name] == 'string')
          values.push(`"${esc(row[header.name])}"`)
        if (typeof row[header.name] == 'number' && typeof row[header.name] == 'boolean')
          values.push(`${row[header.name]}`)
      }
    }

    // формируем строку запроса
    let query = `INSERT INTO ${table} (${cols.join(',')}) VALUES (${values.join(',')})`
    console.log('query: ', query)

    let result = await new Promise( (res, rej) => this._db.run(query, (err, rows) => err ? rej(err) : res(rows) ) )
    return result
  }

  /**
  * Удаляет указанную запись из указанной таблицы
  * @param table имя таблицы
  * @param value значение id, строки с которым удалятся
  * @param search объект строки поиска по которой производится поиск, если не указано value
  * @return result - объект с результатом выполнения
  */
  // TODO: добавить обработку результата выполнения
  async delete({ table, value, search = {} }) {
    // ожидаем готовности подключения
    await this.isReady()

    // проверяем имя таблицы
    this.checkTable(table)

    // если в search что-то есть, выполняем запрос SELECT с этими данными
    let rows = []
    if (value !== undefined)
      rows.push({ id: value })
    if (Object.keys(search).length)
      rows = await this.select({ table, search })

    // возвращаем ошибку, если записи не найдены
    if (rows.length === 0) {
      console.log(`Записи с указанными параметрами поиска не найдены`)
      throw Error(`Записи с указанными параметрами поиска не найдены`)
    }

    // формируем строку запроса
    let query = `DELETE FROM ${table} WHERE ${rows.map(r => 'id='+r.id).join(' or ')}`
    console.log('query: ', query)

    let result = await new Promise( (res, rej) => this._db.run(query, (err, rows) => err ? rej(err) : res(rows) ) )
    return result
  }

  /**
  * Обновляет указанную запись из указанной таблицы
  * @param table имя таблицы
  * @param value значение id, строки с которым удалятся
  * @param search объект строки поиска по которой производится поиск, если не указано value
  * @param row объект строки с заменяемыми данными
  * @return result - объект с результатом выполнения
  */
  // TODO: добавить обработку результата выполнения
  async update({ table, value, search = {}, row = {} }) {
    // ожидаем готовности подключения
    await this.isReady()

    // проверяем имя таблицы
    this.checkTable(table)

    // если в search что-то есть, выполняем запрос SELECT с этими данными
    let rows = []
    if (value !== undefined)
      rows.push({ id: value })
    if (Object.keys(search).length)
      rows = await this.select({ table, search })

    // возвращаем ошибку, если записи не найдены
    if (rows.length === 0) {
      console.log(`Записи с указанными параметрами поиска не найдены`)
      throw Error(`Записи с указанными параметрами поиска не найдены`)
    }

    //
    let sets = []
    for (let r in row) {
      if (r == 'id')
        continue

      if (typeof row[r] == 'string')
        sets.push(`${r}="${esc(row[r])}"`)

      if (typeof row[r] == 'number' && typeof row[r] == 'boolean')
        sets.push(`${r}="${row[r]}"`)


    }

    // формируем строку запроса
    let query = `UPDATE ${table} SET ${sets.join(', ')} WHERE ${rows.map(r => 'id='+r.id).join(' or ')}`
    console.log('query: ', query)

    let result = await new Promise( (res, rej) => this._db.run(query, (err, rows) => err ? rej(err) : res(rows) ) )
    return result
  }
}

module.exports = DB
