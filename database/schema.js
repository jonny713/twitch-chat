/*
* Благодаря данному модулю можно работать с таблицами на уровне кода
* Создание экземпляра класса Table находит соответствие заданной схемы со схемой в базе данных
* В случае расхождения схем, производится резервное копирование файла бд и попытка изменить схему в бд под нужную
*/

const assert = require('assert')
const tableTypes = require('./table-types')
const wait = require('./../modules/utils/wait.js')

const TEMP_TABLE_NAME = 'temp_table'
// let db = {
//   all: _=>_
// }
let db
// const db = new DB()



/**
* Класс схемы таблицы в базе данных.
*/
class Table {
  /*
  * создать таблицу
  */
  static async add({ table }) {

    let queue = table.sql
    console.log(queue)

    let result = await db.request({ query: queue })
    await db.updateTablesList()

    return result
  }

  /**
  * @param schema
  * {
  *   columnName: dataType
  * }
  */
  constructor(tableName, schema = {}) {
    // объявляем объект со всеми столбцами таблицы
    this.columns = {}

    // ставим флаг готовности соединения
    this.ready = false

    // проходим по всем стобцам из схемы
    for (let col in schema) {
      // если в качестве значения для столбца строка, считаем, что это тип
      if (typeof schema[col] == 'string')
        schema[col] = { type: schema[col] }

      // достаем тип
      let { type } = schema[col]

      // проверяем, что тип поддерживается
      if (Table.types[type] === undefined)
        throw Error(`Неизвестный тип данных ${type} для столбца таблицы. Используйте один из следующих: ${Object.keys(Table.types)}`)

      // добавлем стобец в список
      this.columns[col] = new Table.types[type](schema[col])
      // console.log(this.columns[col])
    }

    // выполняем связку с таблицей в БД
    this.connect(tableName)
    return this
  }

  // TODO: переименовать
  async isReady() {
    // if (!this.ready) {
    //   console.log('Подключение к таблице подготавливается')
    //   return new Promise( (res, rej) => setTimeout(_ => res(this.isReady), 50) )
    // }
    return wait(_ => this.ready)
  }

  /*
  * присоединение созданного объекта к опеределенной таблице в БД
  */
  async connect(name) {
    // ждем пока будет подключена база
    await wait(_ => typeof db == 'object')

    // ожидаем готовности подключения
    await db.isReady()

    // запоминаем имя
    this.name = name


    // проверяем, что таблица с указанным именем существует, в противном случае создаем данную таблицу
    try {
      db.checkTable(name)
    } catch (e) {
      console.log('Добавляем таблицу в БД')
      try {
        let result = await Table.add({ table: this })
        console.log('таблица добавлена', result)
      } catch (e) {
        console.log('Добавить не удалось', e)
      }
    }

    let tableFromDb = db.checkTable(name)

    // удостоверяемся, что столбцы в одной и другой таблице совпадают
    if (this.compare(tableFromDb)) {
      //
      this.ready = true

    }


    return this
  }

  /**
  * метод для сверения таблицы в базе данных с заданной схемой
  */
  compare(table) {
    if (table.sql !== this.sql) {
      console.info(`Некоторые столбцы ${this.name} изменены`)
      console.error(`Воспользуйтесь утилитой`)
      console.info('%j', table.columns)
      console.info('%j', this.columns)
      console.info('Таблица в БД:\n%s', table.sql)
      console.info('Модель таблицы:\n%s', this.sql)

      return false
    }

    return true

  }

  get sql() {
    let queue = `CREATE TABLE "${this.name}" (`

    let _queue = []
    for (let c in this.columns) {
      _queue.push(`\n\t"${c}"\t${this.columns[c].sql()}`)
    }

    queue += _queue.join(',') + `\n)`

    // console.log(queue)
    return queue
  }

  selectQueryBuilder(_search) {
    // массив строк с запращиваемыми столбцами
    let cols = []
    // имя таблицы
    let table = this.name
    // массив строк с JOIN'ами
    let joins = []
    // массив поиска
    let searchs = []


    // производим преобразования в соответствии с типами
    for (let col in this.columns) {
      // console.log(this.name, col, _search);
      let { column, search, joined } = this.columns[col].searchHandler(_search ? _search[col] : undefined)

      // добавляем новую строку, если тип позволяет
      if (column !== undefined) {
        cols.push(`${table}.${col}`)
      }

      if (search !== undefined) {
        searchs.push(`${table}.${col}=${search}`)
      }

      if (joined !== undefined && joined instanceof Table) {
        let { cols: _cols, table: _table, joins: _joins, searchs: _searchs } = joined.selectQueryBuilder(_search[joined.name])

        joins.push(`JOIN ${_table} ON ${table}.${col}=${_table}.id`)

        cols.push(..._cols.map(h => `${h} as ${_table}_${h.replace(_table + '.', '')}`))
        searchs.push(..._searchs)
        joins.push(..._joins)
      }
    }

    return { cols, table, joins, searchs }
  }

  /**
  * Поиск определенной записи
  */
  async get(search) {
    let { cols, table, joins, searchs } = this.selectQueryBuilder(search)
    // преобразуем к строке нужного формата
    cols = cols.join(', ')
    joins = joins.length ? ' INNER ' + joins.join(' ') : ''
    searchs = searchs.length ? ' WHERE ' + searchs.join(' and ') : ''

    let query = `SELECT ${cols}\nFROM ${table}${joins}${searchs}`

    // делаем запрос
    let rows = await db.request({ table: this.name, query })
    console.log(`\nQUERY:\n${query}`)

    // видоизменяем результат в соответствии с типами
    for (let row of rows) {
      for (let col in row) {
        if (this.columns[col] !== undefined) {
          // console.log(col)
          row[col] = this.columns[col].loadHandler(row[col])
        }

        if (col.includes('_')) {
          let name = col.split('_')[0]
          let key = col.split('_')[1]
          row[name] = row[name] || {}
          row[name][key] = row[col]
          delete row[col]
        }
      }

    }

    return rows
  }

  /**
  * Поиск определенной записи
  */
  async getOne(search) {
    let arr = await this.get(search)

    return arr[0]
  }

  /**
  * Вставка новой записи
  */
  async insert(row) {
    return await db.insert({ table: this.name, row })
  }

  /**
  * Удаление записи
  */
  async delete(value, search) {
    return await db.delete({ table: this.name, value, search })
  }
}

Table.types = tableTypes


module.exports = (dbName) => {
  db = require('./index.js').connect(dbName)
  return Table
}
