var wsTest;
reConnect();

function reConnect (){
    wsTest = new WebSocket("ws://18.217.70.56:9899");
    wsTest.onopen = function() {
        wsTest.send( JSON.stringify({type: "chat",name: getGet(1),token: ""})  );
    };
    wsTest.onclose = function() {
        setTimeout(reConnect, 2500);
    };

    wsTest.onmessage = function(evt) {
        var data = JSON.parse(evt.data);
        if (data.type == "message"){
            var color = data.color,
                name = data.name,
                source = data.source,
                level = data.level,
                message = data.message;
            if (message != "") {
                $("#log").append('<div class="mesElem" style="display:none;"><span class="meta"  style="color:'+
                                        color+';"><span class="name">'+
                                        name+'</span></span><span class="message">' +
                                        message+'</span></div>');

                $("#log").find("div.mesElem:last").slideDown(300,chekatel ());
            }
        }
        if (data.type == "redraw"){
            var objParam = data.objParam;
            /*
            objParam = {
                "style":{
                    "design":'border: 1px solid black;border-image: url(http://hoes.esy.es/twitch/all1.png)  45 /30 stretch;background: url(http://hoes.esy.es/twitch/imid.png);background-size: cover;',
                    "name":'margin-left: 25px;display: block;',
                    "message":'margin-left: 20px;',
                    "tops":''},
                "values":{
                    "xChat": 4,
                    "yChat": 90,
                    "width":356,
                    "height": 860,
                    "xTop": 1366,
                    "yTop": 860
                    }
            }
            $("#log").css({left: objParam.values.xChat, top: objParam.values.yChat, width: objParam.values.width, height: objParam.values.height});
            */
        }
        if (data.type == "animate"){

        }

    }
}

function chekatel (){ //функция проверяет, не превышают ли блоки с сообщениями установленную высоту
    var height = 500; //стандартная высота которую нельзя превышать
    var realHeight = 0;
    $("#log").find("div").each(function(indx, element){
      realHeight += $(element).outerHeight(true);
    });
    //alert(realHeight);
    if (realHeight>height)
        $("#log").find("div:first").slideUp(4, function(){$(this).detach();chekatel ();});

}

function getGet (i){
    var regexp = /[^=]*=([^&]*)/i;
    var getValue = (!!regexp.exec(document.location.search)) ? regexp.exec(document.location.search)[i] : '';
    return getValue;
}
