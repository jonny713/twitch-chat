animations.onScreen = {
    start: attr => {
        // Реализацию очереди нужно перенести, сделать ее общей
        if (animations.inProgress) return setTimeout(_ => animations.onScreen.start(attr), 10000)
        animations.inProgress = true

        animations.audio.src = `media/${meta.channel.login}/${attr.sound[Math.floor(Math.random() * attr.sound.length)]}`

        if (animations.audioCanPlay !== null) animations.audio.removeEventListener('canplay', animations.audioCanPlay)
        animations.audioCanPlay = () => {
            animations.audio.volume=0.09

            animations.audio.play()
                .catch(e => console.log(e.message))

            $("body").append(`
				<div id="payMSG" class="delete">
					<img src="media/${meta.channel.login}/${attr.img[Math.floor(Math.random() * attr.img.length)]}" id="payMSG_img">
					<span id="payMSG_name">${attr.user}</span>
					<span id="payMSG_text">${attr.message}</span>
				</div>`);
        }

        if (animations.audioEnded !== null) animations.audio.removeEventListener('ended', animations.audioEnded)
        animations.audioEnded = () => animations.onScreen.generate(attr)


        animations.audio.addEventListener('canplay', animations.audioCanPlay);
        animations.audio.addEventListener('ended', animations.audioEnded)
        // Временное ограничение анимации на 15 секунд
        setTimeout(_ => animations.onScreen.end(), 15*1e3)
    },

    end: () => {
        animations.audio.pause()
        animations.inProgress = false;

        animations.audio.removeEventListener('canplay', animations.audioCanPlay)
        animations.audio.removeEventListener('ended', animations.audioEnded)

        animations.audioCanPlay = null
        animations.audioEnded = null

        for (let del of Array.from(document.getElementsByClassName('delete')))
            del.remove()

    },

    generate: attr => {
        if (!attr.audio) return animations.onScreen.end()
        // Зачитывание сообщения временно отключено
		var audio = new Audio(attr.audio)

		audio.addEventListener('canplay', () => {
			audio.volume=0.5
			audio.play()
		});
		audio.addEventListener('ended', () => {
			setTimeout(function() {animations.onScreen.end(audio)},2500);
		});
		audio.src = voicedText;
    }
}


// function onScreen(attr){
// 	if (animationWork)	setTimeout(function() { onScreen(attr) },10000)
// 		else onScreenLogic(attr);
// }
// function onScreenLogic(attr){//(pic,d,op,indx,size,a){ //РќР°С‡РёРЅР°РµС‚ Р°РЅРёРјР°С†РёСЋ РїРѕ РІСЃРїР»С‹РІР°РЅРёСЋ РІСЃСЋРґСѓ РєР°СЂС‚РёРЅРѕРє
// 	var audio = new Audio(),
// 		sound = attr.attr.sound,
// 		img = attr.attr.img,
// 		source = "https://s3.eu-central-1.amazonaws.com/evil-shrimps/images/"+getGet(1)+"/"+img[Math.floor(Math.random() * img.length)],
// 		user = attr.user,
// 		message = attr.message;
// 	audio.addEventListener('loadeddata', function() {
// 		audio.volume=0.09;
// 		audio.play();
// 		animationWork = true;
// 		$("body").append('<div id="payMSG" class="delete"><img src="' + source +
// 						'" id="payMSG_img"><span id="payMSG_name">' + user + '</span><span id="payMSG_text">' + message + '</span></div>');
// 	});
// 	audio.addEventListener('ended', function() {
// 		onScreenStart(attr);
// 	});
// 	audio.src = "https://s3.eu-central-1.amazonaws.com/evil-shrimps/sound/"+getGet(1)+"/"+sound[Math.floor(Math.random() * sound.length)];;
// }
//
// function onScreenStart(attr){//РѕСЃС‚Р°РЅР°РІР»РёРІР°РµС‚ РІСЃРµ showHideRandPos Р°РЅРёРјР°С†РёРё
// 	var audio = new Audio(),
// 		voicedText = attr.audio;
// 	audio.addEventListener('loadeddata', function() {
// 		audio.volume=0.5;
// 		audio.play();
// 	});
// 	audio.addEventListener('ended', function() {
// 		setTimeout(function() {onScreenStop(audio)},2500);
// 	});
// 	audio.src = voicedText;
// }
//
// function onScreenStop(audio){//РѕСЃС‚Р°РЅР°РІР»РёРІР°РµС‚ РІСЃРµ showHideRandPos Р°РЅРёРјР°С†РёРё
// 	audio.pause();
// 	$(".delete").detach();
// 	animationWork=false;
// }
