animations.stopAll = {
    start: attr => {
        for (let a in animations)
            if (animations[a] && typeof animations[a].end == 'function')
                animations[a].end()
    }
}
