//var mass=[[1,2,3],[4,5,6],[7,8,9]]
/*

{
    "style":{
        "design":'border: 1px solid black;border-image: url(http://hoes.esy.es/twitch/all1.png)  45 /30 stretch;background: url(http://hoes.esy.es/twitch/imid.png);background-size: cover;',
        "name":'margin-left: 25px;display: block;',
        "message":'margin-left: 20px;',
        "tops":''},
    "values":{
        "xChat": 4,
        "yChat": 90,
        "width":356,
        "height": 860,
        "xTop": 1366,
        "yTop": 860
        }
}

*/

var wsTest;
var _token = "";
var _name = getGet(1);

let dragBox = []
reConnect();

function reConnect (){
    wsTest = new WebSocket(`ws://${location.hostname}:${location.port}`);
    wsTest.onopen = function() {
        // wsTest.send( JSON.stringify({type: "param",name: _name,token: _token}) );
        wsTest.send( JSON.stringify({
          domain: 'chat',
          data: {
            type: "connect",
            name: _name,
            token: _token
          }
        }) );
    };
    wsTest.onclose = function() {
        setTimeout(reConnect, 2500);
    };

    wsTest.onmessage = function(evt) {
        var data = JSON.parse(evt.data);
        dragBox = [...data.coordinates.chat]
        console.log(dragBox);
        drawExmpl_(	data.enabledChat, data.enabledTops,
                    data.coordinates.chat[0], data.coordinates.chat[1],
                    data.coordinates.chat[2], data.coordinates.chat[3],
                    data.coordinates.tops[0], data.coordinates.tops[0],
                    12, 14,
                    data.style.message, data.style.name, data.style.text);
    }
}

function getGet (i){
    var regexp = /[^=]*=([^&]*)/i;
    var getValue = (!!regexp.exec(document.location.search)) ? regexp.exec(document.location.search)[i] : '';
    return getValue;
}

function drawExmpl_(enableChat,enableTop,x,y,w,h,xTop,yTop,fontMeta,fontMessage,style,stMet,stMes){
    if(!enableChat) $("#psvdChat").css({display:"none"});
    if(!enableTop) $("#psvdTops").css({display:"none"});
    $("#psvdChat").css({left:x/2, top:y/2, width:w/2, height:h/2});
    $("#psvdChat>.x").text("Слева:"+x + "px");
    $("#psvdChat>.y").text("Сверху:"+y + "px");
    $("#psvdChat>.w").text("Ширина:"+w + "px");
    $("#psvdChat>.h").text("Высота:"+h + "px");

    $(".container").attr("style",style);
    $(".tops").attr("style",style);
    $(".meta").attr("style",stMet);
    $(".message").attr("style",stMes);
    $("#psvdTops").css({left:xTop/2, top:yTop/2});

    $(".meta").css("font-size",fontMeta*3/4);
    $(".message").css("font-size",fontMessage*3/4);
}
/*
var valuesArr = [[4,90,356,860,11,16],[14,0,396,820,11,16],[18,20,420,918,11,16],[24,6,418,1028,11,16],[10,10,10,10,11,16]],
     styleMess = [['border: 1px solid black;border-image: url(http://hoes.esy.es/twitch/all1.png)  45 /30 stretch;background: url(http://hoes.esy.es/twitch/imid.png);background-size: cover;','margin-left: 25px;display: block;','margin-left: 20px;','left:1366px;top:860px;'],['background: rgba(100,100,100,0.4);border: 1px rgba(80,80,80,0.2);box-shadow: 0 0 11px rgba(80,80,80,0.2), 0 0 12px rgba(120,120,120,0.4), 0 0 13px rgba(80,80,80,0.2);border-radius: 4px;','margin: 10px;display: inline;color:#ffe4c4;','margin: 5px;text-shadow: 0 0 6px rgba(2,2,2,0.9),0 0 6px rgba(2,2,2,0.9);font-weight: 600;font-family: sans-serif;color: rgb(222,222,222);','left:1362px;top:850px;'],['background: rgba(100,100,100,0.2);border: 1px rgba(80,80,180,0.8);box-shadow: 0 0 11px rgba(80,80,250,0.2), 0 0 12px rgba(120,120,250,0.4), 0 0 13px rgba(80,80,250,0.2);border-radius: 2px;','margin: 10px;display: inline;color:yellow;','margin: 5px;text-shadow: 0 0 6px rgba(222,2,2,0.9),0 0 6px rgba(2,2,2,0.9);font-weight: 600;font-family: sans-serif;color: rgb(222,222,222);','left:1370px;top:586px;'],[' border-image: url(http://hoes.esy.es/twitch/eternal_1.png) 47% 36% /3 repeat;background: url(http://hoes.esy.es/twitch/eternal_.jpg);background-size: 100% 100%;background-repeat: no-repeat;border-width: 22px;box-shadow: none;color: white;text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3), 2px -2px 3px rgba(0, 0, 0, 0.3);','margin-left: 7px;display: block;','margin-left: 20px;','left:1366px;top:860px;'],['']];
*/
$(document).ready(function(){
    $("#hrefChat").val("http://18.217.70.56/chat.html?login="+_name);

    $("#psvdChat").draggable({
        containment:"parent",
        opacity:0.5,
        stop: (event, ui) => {

            // $("#psvdChat>.x").text("Слева:"+parseInt($("#psvdChat").css("left"))*2 + "px");
            // $("#psvdChat>.y").text("Сверху:"+parseInt($("#psvdChat").css("top"))*2 + "px");
            console.log(dragBox);
            dragBox[0] = ui.position.left * 2
            dragBox[1] = ui.position.top * 2

            wsTest.send( JSON.stringify({
              domain: 'chat',
              data: {
                type: "changeParameters",
                name: _name,
                token: _token,
                coordinates: [...dragBox]
            }}))
        }
    })
    $("#psvdChat").resizable({
        ghost:true,
        maxHeight:520,
        maxWidth:940,
        stop: (event, ui) => {
            // $("#psvdChat>.w").text("Ширина:"+ui.size.width*2 + "px");
            // $("#psvdChat>.h").text("Высота:"+ui.size.height*2 + "px");

            dragBox[2] = ui.size.width * 2
            dragBox[3] = ui.size.height * 2

            wsTest.send( JSON.stringify({
              domain: 'chat',
              data: {
                type: "changeParameters",
                name: _name,
                token: _token,
                coordinates: [...dragBox]
            }}))
        }
    })
    $("#varShbln").change(function(){
        //$("#varShbln").val()
        console.log($("#varShbln option").filter(':selected').attr('ind'));
    });

    $("#psvdTops").draggable({ containment:"parent", opacity:0.5, stop:function(event, ui){
        wsTest.send( JSON.stringify({
          domain: 'chat',
          data: {type: "changeTopPos",name: _name,token: _token,pos: {x: ui.position.left*2, y: ui.position.top*2 }}}) );
        } });
});

function drawExmpl(p){}//удалить

function updt(){
    var p = parseInt($("#varShbln option").filter(':selected').attr('ind'));
    var idAct = parseInt($("#varShbln").val());
    var x = parseInt($("#psvdChat").css("left"))*2;
        valuesArr[p][0] = x;
    var y = parseInt($("#psvdChat").css("top"))*2;
        valuesArr[p][1] = y;
    var w = parseInt($("#psvdChat").css("width"))*2;
        valuesArr[p][2] = w;
    var h = parseInt($("#psvdChat").css("height"))*2;
        valuesArr[p][3] = h;
    var stTop = "'left:" + parseInt($("#psvdTops").css("left"))*2 + "px;top:" + parseInt($("#psvdTops").css("top"))*2 + "px;'";
    $.post("command.php",{state: "updateParam",id: idAct, left: x, top: y, width: w, height: h, posTops: stTop},function(data){alert(data+"Схоронено успешно!");});
}

function activate(){
    var idAct = parseInt($("#varShbln").val());
    $.post("command.php",{state: "activateParam",id: idAct,ch:""},function(data){alert(data+"Активирован шаблон!");});
}
