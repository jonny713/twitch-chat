animations.randPos = {
    start: attr => {
        // Реализацию очереди нужно перенести, сделать ее общей
        if (animations.inProgress) return setTimeout(_ => animations.randPos.start(attr), 10000)
        animations.inProgress = true

        if (animations.audioCanPlay !== null)
            animations.audio.removeEventListener('canplay', animations.audioCanPlay)
        animations.audioCanPlay = () => {
    		animations.audio.volume=0.09

            animations.audio.play()
                .catch(e => console.log(e.message))



    		animations.randPos.generate(
                document.getElementsByTagName('body')[0],
                attr.img || [],
                attr.video || [],
                typeof attr.delay == "number" ? attr.delay : 10000);
    	}

        if (animations.audioEnded !== null)
            animations.audio.removeEventListener('ended', animations.audioEnded)
        animations.audioEnded = () => animations.randPos.end()


        animations.audio.addEventListener('canplay', animations.audioCanPlay);
        animations.audio.addEventListener('ended', animations.audioEnded)

        animations.audio.src = `media/${meta.channel.login}/${attr.sound[Math.floor(Math.random() * attr.sound.length)]}`

        // Временное ограничение анимации на 7 минут
        // setTimeout(_ => animations.randPos.end(), 7*60*1e3)
    },

    end: () => {
        animations.audio.pause()
        animations.inProgress = false;

        animations.audio.removeEventListener('canplay', animations.audioCanPlay)
        animations.audio.removeEventListener('ended', animations.audioEnded)

        animations.audioCanPlay = null
        animations.audioEnded = null

        for (let del of Array.from(document.getElementsByClassName('delete')))
            del.remove()

    },

    generate: (parent, img, video, delay) => {
        if (!animations.inProgress) return 0

    	var rand = Math.floor(Math.random() * (img.length + video.length))

    	var width = 20,
    		height = width * 1.5;

        let elem

    	// console.log(rand, img, video)
    	if (rand > img.length){
            elem = document.createElement('video')
            elem.autoplay = true
            elem.loop = true
            elem.src = `media/${meta.channel.login}/${video[rand-img.length]}`

    		// $("body").append(`<video autoplay loop src="media/${meta.channel.login}/${video[rand-img.length]}" style="position: absolute; left: ${left}vw; top: ${top}vh; max-width: ${width}vw; max-height: ${height}vh;" class="delete"></video>`);
    	}else{
            elem = document.createElement('img')
            elem.src = `media/${meta.channel.login}/${img[rand]}`
    		// $("body").append(`<img src="media/${meta.channel.login}/${img[rand]}" style="position: absolute; left: ${left}vw; top: ${top}vh; max-width: ${width}vw; max-height: ${height}vh;" class="delete">`);
    	}

        // position: absolute; left: ${left}vw; top: ${top}vh; max-width: ${width}vw; max-height: ${height}vh;
        // position: absolute; left: ${left}vw; top: ${top}vh; max-width: ${width}vw; max-height: ${height}vh;
        elem.style.position = 'absolute'
        elem.style.maxHeight = `${top}vh`
        elem.style.maxWidth = `${width}vw`
        elem.style.left = `${Math.floor(Math.random() * (100 - width))}vw`
        elem.style.top = `${Math.floor(Math.random() * (100 - height))}vh`

        elem.className = 'delete'

        parent.appendChild(elem)

    	setTimeout(_ => animations.randPos.generate(parent, img, video, delay), delay);
    }
}

// animations.randPos = attr => {
//     // Реализацию очереди нужно перенести, сделать ее общей
//     if (animationWork) return setTimeout(_ => animations.randPos(attr), 10000)
//
//     var audio = new Audio(),
// 		sound = attr.sound,
// 		img = attr.img,
// 		video = attr.video || [],
// 		delay = typeof attr.delay == "number" ? attr.delay : 10000;
//
// 	audio.addEventListener('canplay ', function() {
// 		//setTimeout(function() {randPosStop(audio)},20000);
// 		audio.volume=0.09;
//         audio.play();
// 		animationWork = true;
// 		randPosGenerate(img, video, delay);
// 	});
//     audio.addEventListener('ended', function() {
// 		randPosStop(audio);
// 	});
//     // audio.addEventListener('loadedmetadata', function(e) {
//     //     console.log(e);
//     //
// 	// 	randPosStop(audio);
// 	// });
// 	audio.src = `media/${meta.channel.login}/${sound[Math.floor(Math.random() * sound.length)]}`
//
//     // Временное ограничение анимации на 7 секунд
//     setTimeout(_ => randPosStop(audio), 7*60*1e3)
// }
//
// function randPosGenerate(img, video, delay){
// 	var rand = Math.floor(Math.random() * (img.length + video.length))
//
// 	var width = 20,
// 		height = width * 1.5,
// 		left = Math.floor(Math.random() * (100 - width)),
// 		top = Math.floor(Math.random() * (100 - height));
//
// 	// console.log(rand, img, video)
// 	if (rand > img.length){
// 		$("body").append(`<video autoplay loop src="media/${meta.channel.login}/${video[rand-img.length]}" style="position: absolute; left: ${left}vw; top: ${top}vh; max-width: ${width}vw; max-height: ${height}vh;" class="delete"></video>`);
// 	}else{
// 		$("body").append(`<img src="media/${meta.channel.login}/${img[rand]}" style="position: absolute; left: ${left}vw; top: ${top}vh; max-width: ${width}vw; max-height: ${height}vh;" class="delete">`);
// 	}
//
// 	setTimeout(_ => animationWork ? randPosGenerate(img,video,delay) : 0, delay);
// }
//
// function randPosStop(audio) {
// 	audio.pause();
// 	$(".delete").detach();
// 	animationWork = false;
// }
