var wsTest;
var globalHeight = 500,
    animationWork = false;


const
    meta = {
        channel: location.search.replace('?','').split('&').reduce((a,s) => a = { ...a , [s.split('=')[0]]: s.split('=')[1]}, {}),
    },
    animations = {
        inProgress: false,
        audio: new Audio(),
        audioCanPlay: null,
        audioEnded: null,
    }


function reConnect (){
    wsTest = new WebSocket(`wss://${location.hostname}:${location.port}`)

    wsTest.__send = wsTest.send
    wsTest.send = data => wsTest.__send(JSON.stringify({
      domain: 'chat',
      data
    }))

    wsTest.onopen = function() {
        wsTest.send({
          type: "connect",
          name: getGet(1),
          token: ""
        })
        //setTimeout(wsTest.send( JSON.stringify({type: "",name: getGet(1),token: ""})  ), 2500);
    };
    wsTest.onclose = function() {
        setTimeout(reConnect, 2500);
    };

    wsTest.onmessage = function(evt) {
        var data = JSON.parse(evt.data);
        if (data.type == "message"){
            var color = data.color,
                name = data.name,
                source = data.source,
                level = data.level,
                message = data.message;
            if (message != "") {
                $("#log").append('<div class="mesElem" style="display:none;"><span class="meta"  style="color:'+
                                        color+';"><span class="name">'+
                                        name+'</span></span><span class="message">' +
                                        message+'</span></div>');

                $("#log").find("div.mesElem:last").slideDown(300,chekatel ());
            }
        }
        if (data.type == "animate"){
            // addScript("https://s3.eu-central-1.amazonaws.com/evil-shrimps/scripts/"+data.func+".js",data.func);
            // setTimeout(function(){eval(data.func)(data.attr)}, 1000);
            console.log(data);
            animations[data.func].start(data.attr)

        }

        if (data.type == 'connect' || data.type == 'redraw') {
            // Если чат отключен, скрываем его
            if (!data.enabledChat)
                $("#log").css({display:"none"});

            // Привязываем координаты
            $("#log").css({
                left: data.coordinates.chat[0],
                top: data.coordinates.chat[1],
                width: data.coordinates.chat[2],
                height: data.coordinates.chat[3]
            });
            console.log('Хуман толстяк');

            // Добавляем стили сообщениям
            $("#styleGlagne").text(`
                .mesElem {
                    ${data.style.message}
                }
                .meta {
                    ${data.style.name}
                }
                .message {
                    ${data.style.text}
                }`);
        }

    }
}

function chekatel (){ //функция проверяет, не превышают ли блоки с сообщениями установленную высоту
    var height = globalHeight; //стандартная высота которую нельзя превышать
    var realHeight = 0;
    $("#log").find("div").each(function(indx, element){
      realHeight += $(element).outerHeight(true);
    });
    //alert(realHeight);
    if (realHeight>height)
        $("#log").find("div:first").slideUp(4, function(){$(this).detach();chekatel ();});

}

function getGet (i){
    var regexp = /[^=]*=([^&]*)/i;
    var getValue = (!!regexp.exec(document.location.search)) ? regexp.exec(document.location.search)[i] : '';
    return getValue;
}

$(document).ready(function(){
    setTimeout(reConnect, 2500);
});

function addScript(source,cls){
    if($("script").is("."+cls))	$("."+cls).detach();
    var script = document.createElement('script');
    script.src = source;
    script.className = cls;
    script.async = false; // чтобы гарантировать порядок
    document.head.appendChild(script);
}
