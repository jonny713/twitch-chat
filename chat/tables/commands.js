const Table = require('./../../database/schema.js')()
const streamers = require('./streamers.js')

let commands = new Table('commandsChat', {
  id: 'ID',
  streamersId: {
    type: 'REFERENCE',
    table: streamers,
    notNull: true,
    default: 1
  },
	commandText: {
    type: 'TEXT',
    notNull: true,
    default: 'ping'
  },
	function: {
    type: 'TEXT',
    notNull: true,
    default: 'ping'
  },
	attribute: {
    type: 'OBJECT',
    notNull: true,
    default: {}
  },
	fromAnotherBot: {
    type: 'INTEGER',
    notNull: true,
    default: 0
  }
})

module.exports = commands
