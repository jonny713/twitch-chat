const Table = require('./../../database/schema.js')();

let streamers = new Table('streamers', {
  id: 'ID',
  name: {
    type: 'TEXT',
    notNull: true,
    unique: true
  }
})


module.exports = streamers
