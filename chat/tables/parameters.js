const Table = require('./../../database/schema.js')()
const streamers = require('./streamers.js')

let parameters = new Table('parametersChat', {
  id: 'ID',
	active: {
    type: 'INTEGER',
    notNull: true,
    default: 0
  },
	streamersId: {
    type: 'REFERENCE',
    table: streamers,
    notNull: true
  },
	enabledChat: {
    type: 'INTEGER',
    default: 0
  },
	enabledTops: {
    type: 'INTEGER',
    default: 0
  },
	styleMessage: 'TEXT',
	styleName: 'TEXT',
	styleText: 'TEXT',
	styleTops: 'TEXT',
	coordinatesChat: {
    type: 'ARRAY',
    default: [0,0,100,300]
  },
	coordinatesTops: {
    type: 'ARRAY',
    default: [1366,860,554,220]
  }
})

module.exports = parameters
