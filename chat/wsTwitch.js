
const TWITCH_WS_PATH = 'wss://irc-ws.chat.twitch.tv/'
// const TWITCH_WS_PATH = 'ws://192.168.1.129:9977'

const WebSocket = require('ws')
const request = require('request')
const Viewer = require('./viewer.js')
const Connection = require('./connection.js')
const config = require('../config.js')


class TwitchConnection extends Connection {
    constructor(channelName, handler) {
        super(channelName, 'twitch', handler)
        console.log('Подключение к twitch, канал', channelName);

        this.insertEmoticons = (msg, emotes) => {
    		//emotes~=  58765:8-18,28-38/86:47-56,58-67,69-78
    		if (emotes === '') return msg

    		emotes = emotes.split('/').map(e => {
    			return {
    				id: e.split(':')[0],
    				emoticonText: msg.slice(+e.split(':')[1].split(',')[0].split('-')[0], +e.split(':')[1].split(',')[0].split('-')[1] + 1).replace(/([(){}\[\]$^+-?,./])/g, '\\$1')
    		    }
    		})
    		for (let e of emotes) {
                try {
                    msg = msg.replace(new RegExp(`(\\s*)${e.emoticonText}([\\s\\r])`, 'g'), `$1<img class='emote' alt='${e.emoticonText}' src='https://static-cdn.jtvnw.net/emoticons/v1/${e.id}/1.0'>$2`)
                } catch (error) {
                    console.log(error)
                }
            }
    		return msg
    	}

    }

    initConnection() {
        console.log('Инициализация подключения к вебсокету. Адрес:', TWITCH_WS_PATH);
        this.connection = new WebSocket(TWITCH_WS_PATH);

        this.connection.on('open', () => this.onConnection());
        this.connection.on('error', (err) => console.error(err))
        this.connection.on('unexpected-response', (err) => console.error(err))
        this.connection.on('ping', (err) => console.error(err))
        this.connection.on('pong', (err) => console.error(err))
        this.connection.on('message', dataMessage => this.onMessage(dataMessage))
        this.connection.on('close', (code, reason) => this.onClose(code, reason));
    }

    onConnection() {
        console.log('Подключение открыто')
        this.connection.send(`CAP REQ :twitch.tv/tags twitch.tv/membership twitch.tv/commands`);
        this.connection.send(`PASS oauth:${config.chatBot.token}`);
        this.connection.send(`NICK ${config.chatBot.name}`);
        setTimeout(_ => this.connection.send(`JOIN #${this.channel.name}`), 500);
    }

    onMessage(dataMessage) {
      // console.log("<- " + dataMessage)
      // Разбор сообщений по регулярным выражениям
  		let msg = dataMessage
  			.replace(/^@/, '')
  			.split(/(;|PRIVMSG|WHISPER|PING|PONG|JOIN|ROOMSTATE)/)
  			.filter(m => m!=';')
  			.map(m =>
  				~m.indexOf('=') ? m :
  				~m.search(/(PRIVMSG|WHISPER|PING|PONG|JOIN|ROOMSTATE)/) ? m = `messageType=${m}` :
  				m = `message=${m.substring(m.indexOf(":")+1)}`)
  			.reduce((a,m) => a = {
  				...a,
  				[m.split('=')[0].split('-').map((s,i) => i?s[0].toUpperCase()+s.substring(1):s ).join('')]:m.split('=')[1]
  			}, {})

      if (! 'messageType' in msg) return

  		// console.log(msg)
  		switch (msg.messageType) {
  			case 'PING':
  				this.sendToConnection("PONG", "PONG")
  				break;
  			case 'PRIVMSG':
  				let iliric = false;

          if (msg.message === undefined) return

  				if (~msg.message.indexOf("ACTION")) {
  					msg.message = msg.message.substring(8, msg.message.length - 1)
  					iliric = true
  				}

  				Viewer.join(msg.displayName.toLowerCase(), this.channel.name, 'twitch', msg.badges.split('/')[0]);

  				this.sendToSubscribers({
            name: msg.displayName,
            color: msg.color,
            message: this.insertEmoticons(msg.message, msg.emotes),
            source: 'twitch.png',
            level: msg.badges.split('/')[0] || 'viewer',
            iliric
          });
  				break;
  			case 'WHISPER':
  				Viewer.join(msg.displayName.toLowerCase(), this.channel.name, 'twitch', msg.badges.split('/')[0]);

  				// lookCommand(msg.displayName, msg.message, viewerArr[i]);
  				break;
  		}
    }

    onClose(code, reason) {
      console.info(`Подключение открыто, код ${code}, причина ${reason || 'неизвестна'}`)
      this.initConnection()
    }

    // Метод насильного выключения подключения
    terminate() {
        super.terminate()

        this.connection.terminate();
    }


    // Способ отправить сообщение в чат от имени бота (или отправить служебное сообщение)
    sendToConnection(messageTwitch, type = 'PRIVMSG', whisperTo = 'Jonny713') {
      switch (type) {
  			case 'PRIVMSG':
  				this.connection.send(`:evil_moobot!evil_moobot@evil_moobot.tmi.twitch.tv PRIVMSG #${this.channel.name} : ${messageTwitch}`);
  				break;
  			case 'WHISPER':
  				this.connection.send(`:evil_moobot!evil_moobot@evil_moobot.tmi.twitch.tv PRIVMSG #${this.channel.name} : /w ${whisperTo} ${messageTwitch}`);
  				break;
  			case 'PING':
  				this.connection.send(`PING`);
  				break;
  			case 'PONG':
  				this.connection.send(`PONG`);
  				break;
  		}
    }

    // Рассылка полученных сообщений по подписчикам
    sendToSubscribers(msg) {
        // обработка безусловной команды
        if (msg.message.trim() === "!ping713") return this.sendToConnection("pong713");

        // вызов колбэка каждого подписчика
        for (let subscriber of this.subscribers) {
            subscriber({type: "message", ...msg})
        }
    }


    collectViewers() {
        if (this.active) return setTimeout(_ => this.collectViewers(), 60000)
		request.get(`https://tmi.twitch.tv/group/user/${this.channel.name}/chatters`, (err, res, body) => {
			if (err || !body || res.statusCode != 200) return this.collectViewers()
			let data
			try {
				data = JSON.parse(body)
			} catch (e) {
				console.log("collectViewers error\n", e)
				return this.collectViewers()
			}

			for (let level in data.chatters) {
				data.chatters[level].forEach(viewerName => Viewer.join(viewerName, this.channel.name, "twitch", level))
			}

			console.log(this.channel.name + " имееет на стриме " + Viewer.getByChannel(this.channel.name, "twitch").length);

			setTimeout(_ => this.collectViewers(), 60000)
		});
	}
}


var twitchObject = function (nameAkk, callback) {
	//console.log( " 011001001 ");
	this.active = true

	// Метод насильного выключения подключения
	this.terminate = () => {
		// помечаем соединение как неактивное, чтобы не перезапускать таймеры
		this.active = false

		wsTwitch.terminate();
		// clearInterval(timerReload);

	}


	// периодическая функция
	let collectViewers = _ => {
		request.get(`https://tmi.twitch.tv/group/user/${nameAkk}/chatters`, (err, res, body) => {
			if (err || !body || res.statusCode != 200) return collectViewers()
			let data
			try {
				data = JSON.parse(body)
			} catch (e) {
				console.log("collectViewers error\n", e)
				return collectViewers()
			}

			for (let level in data.chatters) {
				data.chatters[level].forEach(viewerName => Viewer.join(viewerName, nameAkk, "twitch", level))
			}

			console.log(nameAkk + " имееет на стриме " + Viewer.getByChannel(nameAkk, "twitch").length);
			if (this.active)
				setTimeout(_ => collectViewers(), 60000)
		});
	}
	collectViewers()




	function drawMSG(name, color, message, source, level, iliric){
		console.log(source + "-"+ name +": "+ message);
		if (~message.indexOf("!ping")) sendMsgTwitch("pong");
		if (~message.indexOf("!pong")) sendMsgTwitch("ping");
		callback({
			type: "message",
			name, color, message, source, level, iliric
		});
	}



	/**/
	var wsTwitch = new WebSocket('wss://irc-ws.chat.twitch.tv/');

	// TODO: вынести параметры аутентификации в иное место
	wsTwitch.on('open', function open() {
		wsTwitch.send("CAP REQ :twitch.tv/tags twitch.tv/membership twitch.tv/commands");
		wsTwitch.send("PASS oauth:etjyoy1okylski5hlhnvto05755l3x");
		wsTwitch.send("NICK evil_moobot");
		setTimeout(_ => wsTwitch.send("JOIN #"+nameAkk), 500);
	});

	function sendMsgTwitch(messageTwitch, type = 'PRIVMSG', whisperTo = 'Jonny713') {
		console.dir(">\t" + messageTwitch, {depth: 0, colors: 1})


		switch (type) {
			case 'PRIVMSG':
				wsTwitch.send(`:evil_moobot!evil_moobot@evil_moobot.tmi.twitch.tv PRIVMSG #${nameAkk} : ${messageTwitch}`);
				break;
			case 'WHISPER':
				wsTwitch.send(`:evil_moobot!evil_moobot@evil_moobot.tmi.twitch.tv PRIVMSG #${nameAkk} : /w ${whisperTo} ${messageTwitch}`);
				break;
			case 'PING':
				wsTwitch.send(`PING`);
				break;
			case 'PONG':
				wsTwitch.send(`PONG`);
				break;
		}
	}

	// Обработка полученных сообщений
	wsTwitch.on('message', data_message => {
		console.log("<\t" + data_message)
		// Разбор сообщений по регулярным выражениям
		let msg = data_message
			.replace(/^@/, '')
			.split(/(;|PRIVMSG|WHISPER|PING|PONG|JOIN|ROOMSTATE)/)
			.filter(m => m!=';')
			.map(m =>
				~m.indexOf('=') ? m :
				~m.search(/(PRIVMSG|WHISPER|PING|PONG|JOIN|ROOMSTATE)/) ? m = `messageType=${m}` :
				m = `message=${m.substring(m.indexOf(":")+1)}`)
			.reduce((a,m) => a = {
				...a,
				[m.split('=')[0].split('-').map((s,i) => i?s[0].toUpperCase()+s.substring(1):s ).join('')]:m.split('=')[1]
			}, {})

		if (! 'messageType' in msg) return

		// console.log(msg)
		switch (msg.messageType) {
			case 'PING':
				// wsTwitch.send("PONG");
				sendMsgTwitch("PONG", "PONG")
				//setTimeout(_=> sendMsgTwitch("PING", "PING"), 4*60*1e3)
				break;
			case 'PRIVMSG':
				let iliric = false;

				if (~msg.message.indexOf("ACTION")) {
					msg.message = msg.message.substring(8, msg.message.length - 1)
					iliric = true
				}

				Viewer.join(msg.displayName.toLowerCase(), nameAkk, 'twitch', msg.badges.split('/')[0]);

				drawMSG(msg.displayName, msg.color, getEmoticons(msg.message, msg.emotes), 'twitch.png', msg.badges.split('/')[0] || 'viewer', iliric);
				break;
			case 'WHISPER':
				Viewer.join(msg.displayName.toLowerCase(), nameAkk, 'twitch', msg.badges.split('/')[0]);

				// lookCommand(msg.displayName, msg.message, viewerArr[i]);
				break;
		}

	})

	function getEmoticons(msg, emotes) {
		//emotes=  58765:8-18,28-38/86:47-56,58-67,69-78
		if (emotes === '') return msg

		emotes = emotes.split('/').map(e => {
			return {
				id: e.split(':')[0],
				emoticonText: msg.slice(+e.split(':')[1].split(',')[0].split('-')[0], +e.split(':')[1].split(',')[0].split('-')[1] + 1)
		    }
		})
		// console.dir(msg, {depth: 1, colors: 1})
		// console.dir(emotes, {depth: 1, colors: 1})
		for (let e of emotes) {
			msg = msg.replace(new RegExp(`(\\s*)${e.emoticonText}([\\s\\r])`, 'g'), `$1<img class='emote' alt='${e.emoticonText}' src='https://static-cdn.jtvnw.net/emoticons/v1/${e.id}/1.0'>$2`)
		}
		return msg
	}

}

module.exports = TwitchConnection;
