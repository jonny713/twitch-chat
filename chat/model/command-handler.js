let config = require('./../../config.js')
try {
  config = config.chat.commands
} catch (e) {
  throw Error('В файле config.js необходимо указать поле chat.commands')
}

const Commands = require('./commands.js')

const BOT_SYMBOL = '>'
const COMMAND_SYMBOL = '!'

const TABLE = 'commandsChat'

// const DB = require("./../../database")
// const database = DB.connect()

const commandsTable = require('./../tables/commands.js')

class CommandHandler {
  /**
  * создается объект обработчика команд для конкретного стримера
  * @name имя стримера
  */
  constructor(name) {
    this.name = name

    return this.init()
  }

  /**
  * инициализация объекта. Загружается список имен команд
  */
  async init() {
    // let commands = await database.select({
    //   table: TABLE,
    //   search: {
    //     streamers: {
    //       name: this.name
    //     }
    //   }
    // })
    let commands = await commandsTable.get({
      streamers: {
        name: this.name
      }
    })

    this.commands = commands.map(c => c.commandText)

    return this
  }

  /**
  * производится проверка сообщения на наличие в нем команды
  * @param message строка с сообщением
  * @param user имя пользователя, отправившего сообщением
  * @returns {

      }
возвращает объект
  */
  async check(message, user) {
    // console.log('Проверка сообщения на наличие команды');
    let command = ''

    // проверяем категорию пользователя, отправившего сообщение
    let bot = false
    let superUser = false
    // когда сообщение отправляют боты, подразумеваем, что оно содержит команду
    if (config.botNames.includes(user.toLowerCase()))
      bot = user
    // когда команду отправлет один из админов или стример
    if (config.superUserNames.includes(user.toLowerCase()) || this.name.toLowerCase() === user.toLowerCase())
      superUser = user

    // console.log(`  bot = ${bot}; superUser = ${superUser}; user = ${user} `);

    // TODO:
    // реализация отлова любых ключевых слов внутри с помощью регулярных выражений. Пример - хм в дискорд боте

    // проверяем, что в сообщении вообще есть команда
    if (!(bot || message[0] === COMMAND_SYMBOL))
      return false

    // Mikuia:Jonny713 > > просит озвучить !наэкран Привет чатик)
    // разбираем сообщения и вычленяем из него команду и атрибуты
    if (bot) {
      let parsedMessage = message.replace(new RegExp(`(\\S+) ${BOT_SYMBOL}[^${COMMAND_SYMBOL}]*${COMMAND_SYMBOL}(\\S+)(.*)`), '$1_$2_$3').split('_')

      // console.log('parsedMessage', parsedMessage);
      user = parsedMessage[0]
      command = parsedMessage[1]
      message = parsedMessage[2]
    } else {
      let parsedMessage = message.replace(new RegExp(`${COMMAND_SYMBOL}(\\S+)(.*)`), '$1_$2').split('_')

      // console.log('parsedMessage', parsedMessage);
      command = parsedMessage[0]
      message = parsedMessage[1]
    }

    // проверяем наличие команды из сообщения в списке
    if (!this.commands.includes(command))
      return false

    // делаем запрос в бд
    let query = {
      streamers: {
        name: this.name
      },
      commandText: command
    }

    // если сообщение не от бота или суперпользователя, то поле fromAnotherBot должно быть 0
    if (!superUser && !bot) {
      query.fromAnotherBot = 0
    }
    // console.log('query', query);

    try {
      let result = await commandsTable.getOne(query)
      // console.log('result', result);

      if (!result) {
        return false
      }

      // Пропускаем команду через функцию
      let _result = await Commands[result.function]({
        sender: user,
        message,
        attributes: result.attribute
      })

      // console.log(`Вызвана команда ${result.function}. Результат:`)
      // console.dir(_result, { depth: 1, colors: 1 })
      return _result
    } catch (e) {
      // console.error(e)
      return false
    }
  }

}

// this.commands.forEach(command => {
//   // Команда обрабатывается только этим ботом
//   if (!command.fromAnotherBot || ~bots.indexOf(msg.name.toLowerCase()))
//     if (msg.message.indexOf(this.commandSymbol + command.commandText) === 0)
//         commandsHandler[command.function](command, msg, this.name, msg => msg !== undefined ? this.animate(msg) : 0)
//
//   // Команда обрабатывается другим ботом
//   if (command.fromAnotherBot) {
//       if (~msg.message.indexOf(command.commandText)) {
//           let userByBot = msg.message.substring(0, msg.message.indexOf(">")).trim()
//
//           // Сообщение должно быть от одного из списка,
//           // но не должно пересылать от одного из списка
//           if (~bots.indexOf(msg.name.toLowerCase()) && !~bots.indexOf(userByBot.toLowerCase()))
//               commandsHandler[command.function](command, { ...msg, name: userByBot }, this.name, msg => msg !== undefined ? this.animate(msg) : 0)
//       }
//   }
// });

module.exports = CommandHandler
