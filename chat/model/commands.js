
class Commands {
  /**
  * банальная команда пинг
  */
  static async ping({ sender } = {}) {
    return {
  		type: 'animate',
  		func: 'ping',
      sender
  	}
  }

  /**
  * команда для вывода списка гифок под музыку на стриме
  */
  static async randPos({ sender, attributes = {} } = {}) {
    return {
  		type: 'animate',
  		func: 'randPos',
      sender,
  		attr: attributes
  	}
  }

  /**
  * вывода сообщения пользователя на стриме
  */
  static async onScreen({ sender, message, attributes = {} } = {}) {
    // TODO: необходимо дополнительно грузить озвучку текста
    return {
  		type: 'animate',
  		func: 'onScreen',
      sender,
  		attr: {
  			audio: null,
  			message,
  			...attributes
  		}
  	}
  }


  /**
  * команда остановки текущей анимации на стриме
  */
  static async stopAll() {
    return {
  		type: 'animate',
  		func: 'stopAll',
  	}
  }
}

module.exports = Commands
