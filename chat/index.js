const express = require('express');
const router = express.Router()

const streamers = require('./tables/streamers.js')

router.get('/', function (req, res) {
    // console.dir(req.headers, {depth: 0, colors: 1})
    console.log('GET / ', req.query);
    res.render("login")
});

router.use(async (req, res, next) => {
    console.log('auth ', req.body.login || req.query.login || 'логин отсутствует');

    login = req.body.login || req.query.login || ""

    let streamer = await streamers.get({ name: login })

    req.login = streamer[0]
    next()
})
router.post('/', function (req, res) {
    // console.dir(req.headers, {depth: 0, colors: 1})
    console.log('POST / ', req.body);
    login = req.body.login
    res.redirect(`/param?login=${login}`)
});

router.get('/param', function (req, res) {
    console.log('GET /param ', req.query);
    res.render("param")
});


router.get('/chat', function (req, res) {
    console.log('GET /chat ', req.query);
    res.render("chat")
});
module.exports = router;
