const WebSocket = require('ws')
const wsTwitch_ = require("./wsTwitch")

const parameters = require('./tables/parameters.js');

// const commandsHandler = require("../command")
const nanoid = require('nanoid')
const CommandHandler = require('./model/command-handler.js')


// подключенные клиенты
var clients = [];

/*Клиент как объект
*/
class ClientWebSocket {
  static async connect(name, token) {
    try {
      // проверка tokena

      // проверка на дубль
      let client = ClientWebSocket.clients.find(c => c.name === name)
      if (client !== undefined) {
          return client
      }

      // если не дубль, создаем
      client = await new ClientWebSocket(name)
      ClientWebSocket.clients.push(client)
      return client

    } catch (e) {
      console.log(e);
    }
  }

  constructor(name, token, numClient) {

  	this.name = name;
  	this.token = token;
  	this.numClient = numClient;

    this.commandSymbol = '!'
    this.commands = []

    this.callbacks = {}

    wsTwitch_.subcribe(name, msg => this.drawMsg(msg))

    return this.init()
  }

  async init() {
    this.commandHandler = await new CommandHandler(this.name)

    this.parametersChat = await parameters.getOne({
      streamers: {
        name: this.name
      },
      active: 1
    })

    return this
  }

  get parametersChat() {
      return this
  }
  set parametersChat(parameters) {
    console.log("Параметры чата, канал %s", this.name)
    console.dir(parameters, {depth: 0, colors: 1})

    this.active = parameters.active || this.active || 0
    this.streamerId = parameters.streamerId || this.streamerId || 0
    this.enabledChat = parameters.enabledChat || this.enabledChat || 0
    this.enabledTops = parameters.enabledTops || this.enabledTops || 0

    this.style = {}
    this.style = {
        message: parameters.styleMessage || this.style.message || '',
        name: parameters.styleName || this.style.name || '',
        text: parameters.styleText || this.style.text || '',
        tops: parameters.styleTops || this.style.tops || ''
    }

    this.coordinates = {
        chat: parameters.coordinatesChat || this.coordinates.chat || [0, 0, 0, 0],
        tops: parameters.coordinatesTops || this.coordinates.tops || [0, 0, 0, 0]
    }

    return this
  }

  // getParameters(cb = _=>_) {
  //     sql.parametersChat(name, parameters => {
	// 	this.parametersChat = parameters
	// 	cb(parameters);
  //     })
  // }

  subscribe(callback) {
    let uuid = nanoid()
    this.callbacks[uuid] = callback

    callback({
        type: 'connect',
        message: 'Соединение успешно установлено',
        enabledChat: this.enabledChat,
        enabledTops: this.enabledTops,
        style: this.style,
        coordinates: this.coordinates
    })
    return uuid
  }

	unscribe(uuid) {
        delete this.callbacks[uuid]
	}

  reDraw() {
    // if (!this.objParam) return setTimeout(_ => this.reDraw(), 200)
		this.drawMsg ({
      type: "redraw",
      enabledChat: this.enabledChat,
      enabledTops: this.enabledTops,
      style: this.style,
      coordinates: this.coordinates,
    })
	}

	animate(animate) {
		this.drawMsg ({
      type: "animate",
      ...animate
    })
	}

  async drawMsg(msg) {
		if (msg.type == "message") {
      let commandResult = await this.commandHandler.check(msg.message, msg.name)

      if (commandResult)
       this.animate(commandResult)
    }

    for (let cb in this.callbacks) {
        this.callbacks[cb](msg)
    }
	}

  onMessage(message) {
    console.log('\t', message);
    switch (message.type) {
      case 'changeParameters':
        this.parametersChat.coordinates.chat = message.coordinates

        this.reDraw()
        // numOfCallback = nowClient.newCallback(msg => ws.send(JSON.stringify(msg)));
        console.log(`\tСообщение.Изменены параметры `);
        break;

    }
  }

}
ClientWebSocket.clients = []



// module.exports = server => {
//
//     // Инициализируем основной WebSocket-сервер
//     let webSocketServer = new WebSocket.Server({server})
//
//     // При отлавливании нового соединения ищем клиента среди имеющихся
//     // если находим, вызываем соответствующие методы
//     // если не находим, создаем
//     webSocketServer.on('connection', ws => {
//
//         ws._isConnection = false
//
//         // TODO: добавить ожидание сообщения о подписке после onConnection
//         // если после определенного времени подключения нет, то разрываем соединание
//
//
//         // В течение некоторого времени ожидаем от подключившегося по вебсокету
//         // клиента сообщения с аутентификационными данными и типом подключения
//     	ws.on('message', async data => {
//
//             console.log('Сообщение.Получено');
//
//             let message
//             // Пробуем распарсить сообщение
//             try {
//                 message = JSON.parse(data)
//             } catch (error) {
//                 console.log('Неизвестный тип сообщения');
//                 console.log('Сообщение: ', data);
//                 console.log('Ошибка', error);
//                 return ws.send(JSON.stringify({
//                     type: 'error',
//                     message: 'Неизвестный тип сообщения'
//                 }))
//             }
//
//             console.log('Сообщение.Разобрано');
//
//             // проверяем наличие всех необходимых полей
//             let client
//             if ('name' in message && 'token' in message && 'type' in message) {
//                 client = {
//                     name: message.name,
//                     token: message.token,
//                     type: message.type
//                 }
//             }
//             if (client === undefined)
//                 return ws.send(JSON.stringify({
//                     type: 'error',
//                     message: 'Не хватает некоторых полей'
//                 }))
//             console.log('Сообщение.Поля_обнаружены', client);
//
//             // Проверяем, что первое отправленное сообщение имеет тип connect
//             // или подключение уже имеет статус подключенного
//             if (client.type != 'connect' && ws._isConnection == false)
//                 return ws.send(JSON.stringify({
//                     type: 'error',
//                     message: 'Первым делом необходимо отправить сообщение с типом connect и токеном'
//                 }))
//
//             console.log(client.type == 'connect'? 'Соединение.Установка' : 'Соединение.Поиск');
//             // получаем пользователя
//             try {
//                 ws._client = await ClientWebSocket.connect(client.name, client.token)
//                 ws._isConnection = true
//             } catch (e) {
//                 console.log(e);
//                 return ws.send(JSON.stringify({
//                     type: 'error',
//                     message: 'Не удалось создать или найти соединение'
//                 }))
//             }
//
//             console.log(client.type == 'connect'? `Соединение.Создано ${ws._client.name}` : `Соединение.Найдено ${ws._client.name}`);
//
//             // если тип connect, привязываем sender
//             if (client.type == 'connect') {
//                 ws._uuid = ws._client.subscribe(msg => ws.send(JSON.stringify(msg)))
//                 return
//             }
//             console.log(`\tСообщение.Передано ${ws._client.name}#${ws._uuid}`);
//
//             // если любой другой тип, пересылаем сообщение в объект класса ClientWebSocket
//             return ws._client.onMessage(message)
//     	});
//
//     	ws.on('close', _ => {
//             if (ws._client !== undefined && typeof ws._client.unscribe == 'function')
//                 ws._client.unscribe(ws._uuid)
//
//     		console.log(`\tСоединение.Разорвано ${ws._client.name}#${ws._uuid}`)
//     	});
//
//     });
//
// }


module.exports.handler = async (ws, message) => {
      // проверяем наличие всех необходимых полей
      let { name, token, type } = message

      if (name === undefined || token === undefined || type === undefined)
          return ws.send({
              type: 'error',
              message: 'Не хватает некоторых полей'
          })
      console.log('Сообщение.Поля_обнаружены:', { name, token, type });

      // Проверяем, что первое отправленное сообщение имеет тип connect
      // или подключение уже установлено
      if (type != 'connect' && ws._isConnected == false)
          return ws.send({
              type: 'error',
              message: 'Первым делом необходимо отправить сообщение с типом connect и токеном'
          })

      console.log(type == 'connect'? 'Соединение.Установка' : 'Соединение.Поиск');
      // получаем пользователя
      try {
          ws._client = await ClientWebSocket.connect(name, token)
          ws._isConnected = true
      } catch (e) {
          console.log(e);
          return ws.send({
              type: 'error',
              message: 'Не удалось создать или найти соединение'
          })
      }

      console.log(type == 'connect'? `Соединение.Создано ${ws._client.name}` : `Соединение.Найдено ${ws._client.name}`);

      // если тип connect, привязываем sender
      if (type == 'connect') {
          ws._uuid = ws._client.subscribe(msg => ws.send(msg))
          ws.on('close', _ => {
            if (ws._client !== undefined && typeof ws._client.unscribe == 'function')
                ws._client.unscribe(ws._uuid)

        		console.log(`\tСоединение.Разорвано ${ws._client.name}#${ws._uuid}`)
        	})
          return
      }
      console.log(`\tСообщение.Передано ${ws._client.name}#${ws._uuid}`);

      // если любой другой тип, пересылаем сообщение в объект класса ClientWebSocket
      return ws._client.onMessage(message)
}
