class Connection {
    constructor(name, resource, handler) {
        // Помечаем соединение как активное
        this.active = true

        //
        this.channel = {
            name,
            resource
        }

        this.subscribers = [handler]
        console.log('Базовая настройка соединания произведена');
        this.initConnection()
    }

    initConnection() {

    }

    //
    static subcribe(channelName, handler) {
        this.connections = this.connections || []
        console.log(`${this.name} осуществляем подписку на сообщения из канала`, channelName);

        for (let connection of this.connections) {
            if (connection.channel.name == channelName) {
                connection.subscribers.push(handler)
                console.log('Соединение с каналом найдено, подписка на сообщения оттуда произведена');
                return connection
            }
        }
        console.log('Подписка на указанный канал не найдена');
        let newConnection = new this(channelName, handler)
        this.connections.push(newConnection)
        return newConnection
    }

    sendToConnection() {

    }

    sendToSubscribers() {

    }

    // Метод насильного выключения подключения
    terminate() {
        // помечаем соединение как неактивное, чтобы не перезапускать таймеры
		this.active = false
    }

}

module.exports = Connection
