class Viewer {
	constructor(name = 'viewer1', channel = 'broadcaster1', source = 'twitch', level = 'viewer') {
		let duplicateViewer = Viewer.arr.find(v => v.name == name && v.source == source && v.channel == channel)
		if (duplicateViewer) {
            duplicateViewer.level = level
			duplicateViewer.online = true
			duplicateViewer.lastActivity = Date.now()
			return duplicateViewer
		}

		this.name = name
		this.channel = channel
		this.source = source
		this.level = level
		this.online = true
		this.lastActivity = Date.now()

		Viewer.arr.push(this)

        return this
	}

	static join(name, channel, source, level) {
		return new Viewer(name, channel, source, level)
	}
	static controlActivity() {
		let now = Date.now(),
			ttl = 90*1e3,
			ttc = 60*1e3

		Viewer.arr.forEach(viewer => viewer.online && now - viewer.lastActivity > ttl ? viewer.online = false : 0)

		setTimeout(_ => Viewer.controlActivity(), ttc)
	}
	// static offline(name, channel, source, level) {
	// 	let offlineViewer = Viewer.arr.find(v => v.name == name && v.source == source && this.channel == channel)
	// 	if (offlineViewer) {
	// 		offlineViewer.online = false
	// 		return offlineViewer
	// 	}
	// }
	static getByChannel(channel, source) {
		return Viewer.arr.filter(v => v.source == source && v.channel == channel && v.online)
	}

}
Viewer.arr = []
Viewer.controlActivity()

module.exports = Viewer
