/**
 Набор различных тестов сообщений со стороны twitch
*/

const logger = require('./../modules/log.js')()

// запускаем web-socket сервер, который будет эмулировать twitch

const app = require('express')()
let server = app.listen(9977)
app.get('*', (req, res) => res.send("<script>let ws = new WebSocket('ws://localhost:9977')</script>"))

const WebSocket = require('ws')
let testWS = new WebSocket.Server({ server })

testWS.on('connection', ws => {
  console.log('Connect')

  // ws.send('test')

  ws.on('message', message => {
    console.log(message)

    if (message.includes('JOIN #')) {
      for (let msg of test(message.replace(/.*#/, ''))){
        console.log('send ', msg)
        ws.send(msg)
      }
    }
  })
})

// функция отправки сообщения по веб сокету
let sendToWS = ({ name = 'Shrimps_BOT', room = 'jonny713', msg = '0' } = {}) => {
  let str = `@badge-info=;badges=moderator/1;color=#FF0000;display-name=${name};emotes=;flags=;id=;mod=1;room-id=118943649;subscriber=0;tmi-sent-ts=1580397734914;turbo=0;user-id=136502091;user-type=mod :${name}!${name}@${name}.tmi.twitch.tv PRIVMSG #${room} :${msg}`
  return str
}

function test(room) {
  let msgs = []
  // приветственное сообщение
  msgs.push(sendToWS({ msg: 'Привет, это тест', room }))

  // тест команды от пользователя
  // msgs.push(sendToWS({ name: 'user', msg: '!dance 123', room }))

  // тест команды от бота
  msgs.push(sendToWS({ name: 'mikuia', msg: 'user > просит озвучить !dance 123', room }))


  return msgs
}
