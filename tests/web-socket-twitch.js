
class WebSocket {
    constructor() {
        this.status = 0
        this.statuses = [
            'Неопределенное',
            'Подписан на open',
            'Подписан на message',
            'Подписан на close',
            'Отправил init сообщения',
            'Отправил данные аутентификации',
            'Присоединился к комнате',
            'Тест пройден',
        ]
    }

    on(type, cb) {
        let types = [
            'open',
            'message',
            'close'
        ]

        if (!~types.indexOf(type))
            return console.error('Неизвестный тип');

        switch(type) {
            case 'open':
                break
            case 'message':
                break
            case 'close':
                break
        }
    }


    send(message) {

    }


    terminate() {

    }
}


module.exports = WebSocket
