const { Client, Intents, MessageActionRow, MessageButton, MessageEmbed } = require('discord.js');
const { v4: uuidv4 } = require('uuid');
/**
  * Соединяется ...
*/

function getResponse(message) {
  const res = {}

  res.send = (content) => {
    message.channel.send(content)
  }

  res.sendImage = (imageSrc, content) => {
    const embed = new MessageEmbed()
    if (imageSrc) {
      embed.setImage(imageSrc)
      message.embeds.push(embed)
    }
    if (content) {
      message.content = content
      // embed.setDescription(content)
    }
    message.reply(message)
  }

  // формируем уникальный префикс для имен кнопок, созданных в рамках одной команды
  res.uniqueId = uuidv4()
  // общий список кнопок
  res.buttons = {}

  /**
    * Добавляет кнопку
    * {
    * @param {String} buttonName имя кнопки
    * @param {String} style стиль кнопки. по умолчанию - 'SECONDARY'. см. "https://discord.com/developers/docs/interactions/message-components#button-object-button-styles"
    * }
    * @param {function} callback функция, вызываемая при нажатии кнопки
  */
    res.addButton = ({buttonName, style = 'SECONDARY'}, callback) => {
      const button = new MessageButton()
      .setCustomId(res.uniqueId + '_' + buttonName)
      .setLabel(buttonName)
      .setStyle(style)
      res.buttons[res.uniqueId + '_' + buttonName] = {button, callback};
    }
    // добавляем кнопки в сообщение и посылаем его
    res.sendButtonsGroup = (content) => {
      // поскольку в Discord нельзя создать объект MessageActionRow, содержащий больше пяти кнопок
      // (см. "https://discord.com/developers/docs/interactions/message-components#buttons")
      // создаем массив элементов типа MessageActionRow с пятью кнопками каждый
      const rows = []

      // массив, содержащий индексы кнопок, кратные пяти
      var rowsBeginnings = []
      // массив, содержащий все кнопки
      const buttonsList = Object.values(res.buttons)
      // добавляем индексы кнопок, кратные пяти
      buttonsList.filter((b,index) => !(index % 5))
        .forEach(button => rowsBeginnings.push(buttonsList.indexOf(button)))

      // формируем массив объектов, содержащих группы из пяти или менее кнопок
      // количество кнопок в "полных" (пятикнопочных) группах
      let fullRowsAmount = (rowsBeginnings.length - 1) * 5
      // общее число групп кнопок (учитавется в том числе и последняя, которая может оказаться неполной)
      let rowsAmount = fullRowsAmount < buttonsList.length ? rowsBeginnings.length + 1 : rowsBeginnings.length
      // формируем объекты MessageActionRow и добавляем их в общий массив
      for ( let j = 1; j < rowsAmount; j++ ) {
        rows.push(new MessageActionRow()
          .addComponents(buttonsList.filter((b,index) => {
            // индекс последней кнопки j-го объекта MessageActionRow в общем массиве кнопок buttonsList
            let lastIndex = j >= rowsBeginnings.length ? buttonsList.length : rowsBeginnings[j];
            // индекс первой кнопки j-го объекта MessageActionRow в общем массиве кнопок buttonsList
            let startingIndex = j > 1 ? rowsBeginnings[j-1] : rowsBeginnings[0]
            return index < lastIndex && index >= startingIndex
          })
          .map(butt => butt.button)
        ))}

      // выбираем только те кнопки, которые соответствуют набору вызывающей команды
      const filter = i => i.customId.startsWith(res.uniqueId);
      // формируем из них cборщик событий
      const collector = message.channel.createMessageComponentCollector({ filter });

      // нажатие на клавишу, обновляем сборщик
      collector.on('collect', async interaction => {
        const updatedContent = res.buttons[interaction.customId].callback(collector, interaction)
        await interaction.update({ content: updatedContent, components: rows })
      });

      // завершаем сбор событий
      collector.on('end', collected => {
        rows.forEach(row => row.components.forEach(button => button.setDisabled()))
          console.log(`Collected ${collected.size} items`)
        })
      // посылаем сообщение с набором кнопок
      message.reply({ content: content, components: rows });
    }

  return res
}



class DiscordHandler {
  intervals = {}

  constructor(token, charCommand = '!') {
    this.commandHandlers = []
    this.charCommand = charCommand

    const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] })
    client.login(token)

    client.once('ready', () => console.log('Discord bot is ready'))
    client.on('error', err => console.error(err))

    client.on('messageCreate', (message) => {
      if (client.user.username == message.author.username) {
        return
      }

      for (let hanlder of this.commandHandlers) {
        hanlder(message, getResponse(message))
      }
    })

    client.on('interactionCreate', interaction => {
      if (!interaction.isButton()) return;

    });
  }


  /**
  * Добавляет команду вида "!команда" для обработки
  */
  command(commandName, responseFunction) {
    this.commandHandlers.push(({ content, author }, res) => {
      // Если сообщение не начинается с коммандного символа, игнорируем
      if (content[0] !== this.charCommand)
        return

      // Сверяем команду
      if (content.indexOf(this.charCommand + commandName) !== 0)
        return

      const params = content.split(" ").filter((a,i) => !!i)
      responseFunction({ content, author, params }, res)
    })
  }
  /**
  * Добавляет команду с полнотекстовым поиском для обработки
  */
  regex(commandRegexName, responseFunction) {
    // Формируем регулярное выражение из строки
    let regex = new RegExp(commandRegexName, 'i')

    this.commandHandlers.push(({ content, author }, res) => {
      // Сверяем команду
      if(!regex.test(content))
        return

      // Формируем параметры, если они есть
      const params = regex.exec(content) ? regex.exec(content).filter((a,i) => !!i) : []
      responseFunction({ content, author, params }, res)
    })
  }
  /**
  * Добавляет периодечески повторяющуюся команду
  * @param {String} commandName имя команды
  * @param {Number} period период, через который происходит повторение функции
  * @param {function} responseFunction функция, которая будет повторяться
  */
  repeat(commandName , period, responseFunction) {
    // период должен быть не менее 1000
    period = ( period < 1000 ) ? 1000 : period

    // при повторном введении команды повторение останавливается
    this.command(commandName, (req, res) => {
      if  ( this.intervals[commandName] ) {
        clearInterval(this.intervals[commandName])
        delete this.intervals[commandName]
        return
      }

      // запускается периодеческое повторение и запоминается имя вызваной команды
      let repeats = 0
      this.intervals[commandName] = setInterval(() => {
        if (repeats < 15) {
          responseFunction(req, res)
          repeats++
        } else {
          repeats = 0
          clearInterval(this.intervals[commandName])
          delete this.intervals[commandName]
        }
      }, period)
    })
  }
}

module.exports = DiscordHandler
